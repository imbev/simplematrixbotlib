The simplematrixbotlib package can be installed from pypi or from the git repository.

### Installing from PyPi
Run the following command in your terminal or cmd prompt to install simplematrixbotlib from pypi
```
python -m pip install simplematrixbotlib
```

See [Encryption](#requirements) to lean how to install E2E encryption support.

### Installing from Git Repo
Run the following command in your terminal or cmd prompt to download the repository to your machine.
```
git clone --branch master https://codeberg.org/imbev/simplematrixbotlib.git
```
The package is located under (current directory)/simple-matrix-bot-lib as simplematrixbotlib.
